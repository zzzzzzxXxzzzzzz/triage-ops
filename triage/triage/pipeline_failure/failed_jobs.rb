# frozen_string_literal: true

require_relative '../../../triage/triage'

module Triage
  module PipelineFailure
    class FailedJobs
      def initialize(event)
        @event = event
      end

      def execute
        failed_jobs = []

        Triage.api_client.pipeline_jobs(event.project_id, event.id, scope: 'failed', per_page: 100).auto_paginate do |job|
          next if job.allow_failure

          failed_jobs << job
        end

        Triage.api_client.pipeline_bridges(event.project_id, event.id, scope: 'failed', per_page: 100).auto_paginate do |job|
          next if job.allow_failure

          job.web_url = job.downstream_pipeline.web_url # job.web_url is linking to an invalid page
          failed_jobs << job
        end

        failed_jobs
      end

      private

      attr_reader :event
    end
  end
end
