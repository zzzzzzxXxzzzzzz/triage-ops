# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'

module Triage
  module Workflow
    class ClosedIssueWorkflowLabelUpdater < Processor
      react_to 'issue.close'

      def applicable?
        event.from_gitlab_org? &&
          event.label_names.include?(Labels::WORKFLOW_VERIFICATION)
      end

      def process
        post_comment_and_update_workflow_label
      end

      def documentation
        <<~TEXT
        This processor changes closed issues from workflow::verification to verification::complete
        TEXT
      end

      private

      def post_comment_and_update_workflow_label
        comment = <<~MARKDOWN.chomp
          @#{event.event_actor_username}, the workflow label was automatically updated to ~"#{Labels::WORKFLOW_COMPLETE}" because you closed the issue while in ~"#{Labels::WORKFLOW_VERIFICATION}".

          If this is not the correct label, please update.

          To avoid this message, update the workflow label as you close the issue.
          /label ~"#{Labels::WORKFLOW_COMPLETE}"
        MARKDOWN

        add_comment(comment, append_source_link: false)
      end
    end
  end
end
